This sample was used to generate the CPU benchmarks for 
Väisälä et al. "Interaction of large- and small-scale dynamos in isotropic homogeneous turbulent flows", 2020.


# Setting up the test case
	- Clone Pencil Code from https://github.com/pencil-code/, commit 'b2f1ac88de3e9ba81075f519cf8269dd075f3d4b'
	- Copy helical-MHDturb to pencil-code/samples, overwriting if necessary

# Building and running
	- cd pencil-code
	- source ./sourceme.sh
	- cd samples/helical-MHDturb
	- pc_setupsrc
	- make -j
	- ./start.csh
	- sbatch -n 40 -N 1 <optional parameters> ./run.csh

> The sample in helical-MHDturb is modified from a sample provided by Pencil Code
(C) NORDITA. Pencil Code is licenced under GNU General Public Licence V2. See
https://github.com/pencil-code/pencil-code/tree/master/license for more
details.
